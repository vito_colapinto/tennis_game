package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void test() {
		//Arrange
		Player player= new Player("Federer",0);
		player.incrementScore();
		//Assert
		assertEquals(1,player.getScore());
		
	}

	@Test
	public void scoreShouldbelove() {
		//Arrange
		Player player= new Player("Federer",0);
		
		String scoreAsString=player.getScoreAsString();
		//Assert
		assertEquals("love",scoreAsString);
		
	}
	
	
	
	@Test
	public void scoreShouldbetie() {
		//Arrange
		Player player1= new Player("Federer",2);
		Player player2= new Player("Nadal",2);
		
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertTrue(tie);
		
	}
	
	@Test
	public void scoreShouldnotbetie() {
		//Arrange
		Player player1= new Player("Federer",3);
		Player player2= new Player("Nadal",2);
		
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertFalse(tie);
		
	}
	
	@Test
	public void scoreShouldHaveLessThanFortyPoints() {
		//Arrange
		Player player1= new Player("Federer",3);
		
		boolean outcome = player1.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
		
	}
}
